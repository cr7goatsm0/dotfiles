#!/bin/bash

xrdb -merge ~/.Xresources &
/usr/lib/mate-polkit/polkit-mate-authentication-agent-1 &
nitrogen --restore &
setxkbmap -layout gb &
#xinput set-prop "ETPS/2 Elantech Touchpad" "libinput Tapping Enabled" 1
