## Dotfiles

![dekstop](/uploads/b9cdbd4fc8c760f669b5040c52ae578a/dekstop.png)

This repository contains the dotfiles I use on my personal Linux system. The dotfiles include configuration files for window managers and applications; you will also find scripts (mostly in the bin directory) that I use to perform a range of tasks. Whilst this GitLab repository was created so that I can access my dotfiles whenever I install or reinstall a system, you are free to use them under the terms of an MIT Licence. However, you should be aware that these files are often a work in progress so I make no guarantees about how well they will work for you.

## Who Am I?

My real name is Steve Anelay, but most people just call me OTB. I am the creator of the OldTechBloke Youtube channel ([https://www.youtube.com/c/OldTechBloke](https://www.youtube.com/c/OldTechBloke)). I&#39;m a Linux desktop enthusiast who has been installing and running Linux since 2004. I am definitely not a developer like many on GitLab, just a desktop user with a passion for all things Linux. My videos cover a range of topics, from distro reviews through to commentaries and Linux news items and debates.

## Licence

The content of this repository is licensed under the MIT Licence. Basically, that gives you the right to use, copy, modify, etc. the content how you see fit. You can read the full licence terms here ([https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)).
